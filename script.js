import{Programmer} from "./Programmer.js"

const programmerFirst = new Programmer('Tanya', 25, 2000, "Ruby, Go");
console.log(programmerFirst);

const programmerSecond = new Programmer('Maryna', 45, 5000, "Python, JS, Java");
console.log(programmerSecond);
